//test the addition function from index
const addition = require('../index');

describe('addition', () => {
    it('should add two numbers', () => {
        expect(addition(1, 2)).toEqual(3);
    });

    it('should add two numbers', () => {
        expect(addition(1, 5)).toBe(3);
    });
}
);